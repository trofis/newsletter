# Build stage
FROM node:lts-alpine
WORKDIR .
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build
RUN npm run preview