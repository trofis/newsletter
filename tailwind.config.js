/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,vue}"],
  theme: {
    extend: {
      width: {
        520: "520px",
      },
    },
  },
  plugins: [],
};
