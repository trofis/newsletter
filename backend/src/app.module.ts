import { Module } from '@nestjs/common';
import { NewsLetterModule } from './modules/newsletter/newsletter.module';

@Module({
  imports: [NewsLetterModule],
})
export class AppModule {}
