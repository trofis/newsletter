import { Injectable } from '@nestjs/common';
import { NewsLetterMock } from './mock/newsletters.mock';

@Injectable()
export class NewsLetterService {
  constructor() {}

  async findAll(): Promise<any> {
    return NewsLetterMock;
  }
}
