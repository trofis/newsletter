export const NewsLetterMock = [
  {
    id: 1,
    title: 'Atelier peinture',
    date: '2023-11-01',
    content:
      'Les enfants ont exploré leur créativité lors de notre atelier peinture hebdomadaire.',
    photos: [
      'https://source.unsplash.com/random/200x200?kids,painting',
      'https://source.unsplash.com/random/201x201?kids,painting',
    ],
    author: 'Alice Martin',
    avatar: 'https://source.unsplash.com/random/50x50?portrait',
  },
  {
    id: 2,
    title: 'Sortie au parc',
    date: '2023-10-30',
    content: 'Nous avons profité du beau temps pour une sortie au parc local.',
    photos: ['https://source.unsplash.com/random/202x202?kids,park'],
    author: 'Bob Dupont',
    avatar: 'https://source.unsplash.com/random/51x51?portrait',
  },
  {
    id: 3,
    title: 'Spectacle de marionnettes',
    date: '2023-10-29',
    content:
      "Les enfants ont adoré le spectacle de marionnettes organisé par l'équipe pédagogique.",
    photos: [
      'https://source.unsplash.com/random/203x203?puppet,show',
      'https://source.unsplash.com/random/204x204?puppet,show',
      'https://source.unsplash.com/random/205x205?puppet,show',
    ],
    author: 'Charlie Durand',
    avatar: 'https://source.unsplash.com/random/52x52?portrait',
  },
];
