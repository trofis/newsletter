import { Controller, Get } from '@nestjs/common';
import { NewsLetterService } from './newsletter.service';

@Controller('newsletter')
export class NewsLetterController {
  NewsLetterService: NewsLetterService;
  constructor(NewsLetterService: NewsLetterService) {
    this.NewsLetterService = NewsLetterService;
  }

  @Get('')
  async findAll() {
    return this.NewsLetterService.findAll();
  }
}
